using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubemove : MonoBehaviour
{
    public Transform cube;
    void Start()
    {
            
    }

    
    void Update()
    {
        transform.Translate(new Vector3(0, 0, 1) * TextParams.speed * 10 * Time.deltaTime);
        if (transform.position.z >= (TextParams.distance * 50))
            Destroy(cube.gameObject);
    }

    public void Destroy()
    {
        
    }
}
