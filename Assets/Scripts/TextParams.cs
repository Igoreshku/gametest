using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TextParams : MonoBehaviour
{
    [SerializeField] private TMP_Text InputTextSpeed;
    [SerializeField] private TMP_Text InputTextDistance;
    [SerializeField] private TMP_Text InputTextTimes;
    [SerializeField] private GameObject Menu;
    [SerializeField] private GameObject ParamsGame;
    [SerializeField] public TMP_Text OutputTextSpeed;
    [SerializeField] public TMP_Text OutputTextDistance;
    [SerializeField] public TMP_Text OutputTextTimes;

    public static int speed;
    public static int distance;
    public static int time;
    public  int speed1;
    public  int distance2;
    public  int time3;

    void Start()
    {
        
    }

    
    void Update()
    {
        speed1 = speed;
        distance2 = distance;
        time3 = time;
    }

    public void Play()
    {
        Menu.SetActive(false);
        ParamsGame.SetActive(true);
        OutputTextSpeed.text = InputTextSpeed.text;
        OutputTextDistance.text = InputTextDistance.text;
        OutputTextTimes.text = InputTextTimes.text;
        speed = Convert.ToInt32(OutputTextSpeed);
        distance = Convert.ToInt32(OutputTextDistance);
        time = Convert.ToInt32(OutputTextTimes);
    } 
    
}
